import { Component } from '@angular/core';
import { QuestionService } from '../service/question.service';
import {interval} from 'rxjs';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {

  public name: string="";
  public questionList : any = [];
  public currentQuestion: number = 1;
  public points: number = 0;
  counter = 60;
  numberOfCorrectAnswersGiven: number = 0;
  numberOfIncorrectAnswersGiven: number = 0;
  // $ because its an observable
  interval$ : any;
  progress: string = '';
  isQuizCompleted: boolean = false;


  constructor(private questionService: QuestionService){}

  ngOnInit(): void{
    // ! at the end implies that there'll be some value in the local storage.
    // If we remove it, it is gonna throw an error of name is empty. This is a typescript feature
    // not angular.

    this.name = localStorage.getItem("name")!;
    this.getAllQuestions();
    this.startCounter();
  }

  getAllQuestions(){
    // this is an observable. How is it an observable????
    this.questionService.getQuestionJson()
    .subscribe(res=>{
      this.questionList = res.questions;
  })
  }

  nextQuestion(){
    this.currentQuestion ++;
  }

  previousQuestion(){
    this.currentQuestion --;
  }

  answer(currentq:number, option: any){

    if(currentq === this.questionList.length-1){
      this.isQuizCompleted = true;
      this.stopCounter();
    }
    if(option.correct){
      this.points += 10;
      this.numberOfCorrectAnswersGiven += 1
      
      setTimeout(() =>{
        this.currentQuestion ++;
        this.resetCounter();
        this.getProgressPercent();
      }, 1000);

      
    }else{
      
      this.points -= 10;

      setTimeout(() =>{
        this.currentQuestion ++;
        this.numberOfIncorrectAnswersGiven --;
        this.resetCounter();
        this.getProgressPercent();
      }, 1000);

    }

  }

  startCounter(){
    // interval(1000) => generate a value every 1sec
    this.interval$ = interval(1000)
    .subscribe(val=>{
      // Meaning, whenever rxjs produces a value(which is every 1 sec)
      // subtract 1 from the counter variable(60), as the timer counts down from 60->0
      this.counter --;

      // what happens when counter = 0?
      if(this.counter === 0){
        // 1. to move to the next question increment the currentQuestion counter
        // 2. reset the counter to 60 for the next question
        //3. Decrement the points
        this.currentQuestion++;
        this.counter = 60;
        this.points -= 10;
      }
    });

    // unsubscribing implies that we only want the timer to run for 10mins(not always), as there
    // are 9 questions
    setTimeout(() => {
      this.interval$.unsubscribe();
    }, 600000)
  }

  stopCounter(){
    this.interval$.unsubscribe();
    this.counter = 0;
  }

  resetCounter(){
    this.stopCounter();
    this.counter = 60;
    this.startCounter();
  }

  resetQuiz(){
    this.resetCounter();
    this.getAllQuestions();
    this.points = 0;
    this.counter = 60;
    this.currentQuestion = 0;
    this.progress = '0';
  }

  getProgressPercent(){
    this.progress = ((this.currentQuestion/this.questionList.length)*100).toString();
    return this.progress;
  }
}
