import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {

  // line 11 of the input tag(#) 'name' must be the same as the textbox elements name which is used to
  // take the input of the user who starts the quiz.
  @ViewChild('name') nameKey!: ElementRef;
  constructor(){}

  ngOnInit(): void{

  }
  startQuiz(){
    localStorage.setItem("name", this.nameKey.nativeElement.value);
  }
}
